from pymongo import MongoClient

client = MongoClient('localhost', 27017)
# client = pymongo.MongoClient()
db = client["yasaswi"]
collection = db["user_details"]


class mongo_utilityClass:
    @staticmethod
    def mongo_insertion(data):
        if isinstance(data, list):
            collection.insert_many(data)
            print("Inserted")
        else:
            collection.insert_one(data)
            print("Inserted")

    @staticmethod
    def mongo_find():
        data1= []
        for data in collection.find():
            data1.append(data)
        return data1

    @staticmethod
    def mongo_find_one():
        one = collection.find_one()
        return one

    @staticmethod
    def mongo_update():
        my_query = {"first_name": "Dix"}
        new_values = {"$set": {"first_name": "shyam"}}
        collection.update_one(my_query, new_values)
        ans = []
        for data in collection.find({}, {"first_name": 1}):
            ans.append(data)
        return ans

    @staticmethod
    def mongo_delete():
        myquery = {"first_name": "Roselin"}
        ans = []
        collection.delete_one(myquery)
        for data in collection.find({}, {"first_name": 1}):
            ans.append(data)
        return ans

# json data insertion in mongodb


# record_created = collection.insert_many(data)
# if record_created:
#     return 'Record Created'
