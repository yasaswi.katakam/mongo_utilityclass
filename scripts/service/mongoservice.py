from flask import Blueprint, request
#from scripts.handler.mongohandler_class import mongo_insertion
from scripts.handler.mongohandler_class import mongo_utilityClass

insertion = Blueprint('insertion_blueprint', __name__)

@insertion.route('/', methods=['POST'])
def post_data():
    content = request.get_json()
    mongo_utilityClass.mongo_insertion(content)
    return str(content)


@insertion.route('/mongo_finds', methods=['GET'])
def mongo_finds():
    find1 = mongo_utilityClass.mongo_find()
    return str(find1)


@insertion.route('/mongo_find_ones', methods=['GET'])
def mongo_find_ones():
    find_one = mongo_utilityClass.mongo_find_one()
    return find_one


@insertion.route('/mongo_updates', methods=['GET'])
def mongo_upadates():
    update = mongo_utilityClass.mongo_update()
    return update


@insertion.route('/mongo_deletes', methods=['GET'])
def mongo_deletes():
    delete = mongo_utilityClass.mongo_delete()
    return delete
